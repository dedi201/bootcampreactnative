// jawaban soal 1

function teriak () {
    return "Hallo Sanbers";
}

console.log(teriak());

//jawaban soal 2
function kalikan(num1,num2){
    return num2 * num1;
}
 
var hasilKali = kalikan(4, 12)
console.log(hasilKali) // 48

// jawaban soal 3
function introduce(name, age, address, hobby){
    return `Nama saya ${name}, umur saya ${age} tahun, alamat saya di ${address}, dan saya punya hobby yaitu ${hobby}!`
}

 
var perkenalan = introduce("Agus",30,"Jln. Malioboro, Yogyakarta","Gaming")
console.log(perkenalan)
function readBooksPromise(time,book){
    console.log(`saya mulai membaca ${book.name}`)
    return new Promise(function (resolve, reject){
        setTimeout(function(){
            let siswaWaktu = time - book.timeSpent
            if(siswaWaktu >= 0){
                console.log(`saya sudah selesai membaca ${book.name}, 
                sisa waktu saya ${siswaWaktu}`)
                resolve(siswaWaktu)
            }else {
                console.log(`saya sudah tidak punya waktu untuk
                baca ${book.name}`)
            }   
        },book.timeSpent)
    })
}

module.exports = readBooksPromise 
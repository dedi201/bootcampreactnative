function readBooks(time, book, callback) {
    console.log(`saya membaca ${book.name}`)
    setTimeout(function(){
        let siswaWaktu = 0
        if(time > book.timeSpent) {
            siswaWaktu = time - book.timeSpent
            console.log(`saya sudah membaca ${book.name}, sisa
            waktu saya ${siswaWaktu}`)
            callback(siswaWaktu)
        } else {
            console.log('waktu saya habis')
            callback(time)
        }
    }, book.timeSpent)
}

module.exports = readBooks
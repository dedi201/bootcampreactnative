function range(startNum, finishNum) { 
    var rangeArr = []; 

    //  var rangeLength = Math.abs(startNum - finishNum) + 1; 
     if (startNum > finishNum) { 
         var rangeLength = startNum - finishNum + 1; 
         for (var i = 0; i < rangeLength; i++) { 
             rangeArr.push(startNum - i) 
            } 
        } else if (startNum < finishNum) { 
            var rangeLength = finishNum - startNum + 1; 
            for (var i = 0; i < rangeLength; i++) { 
                rangeArr.push(startNum + i) 
            } 
        } else if (!startNum || !finishNum) { 
            return -1 
        } 
        return rangeArr 
    }

console.log(range(1, 10)) //[1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
console.log(range(1)) // -1
console.log(range(11,18)) // [11, 12, 13, 14, 15, 16, 17, 18]
console.log(range(54, 50)) // [54, 53, 52, 51, 50]
console.log(range()) // -1 

//jawaban soal 2

function rangeWithStep(startNum, finishNum, step){
	var rangerArr = []
	if(startNum > finishNum){
	var currentNum = startNum;
	for (var i = 0; currentNum >= finishNum; i++){
	rangerArr.push(currentNum)
	currentNum -= step
	}
	}else if(startNum < finishNum){
		var currentNum = startNum;
		for(var i = 0; currentNum <= finishNum; i++){
		rangerArr.push(currentNum)
		currentNum += step
		}
	}else if (!startNum || !finishNum || !step){
	return -1
	}
	return rangerArr
	}
	
	console.log(rangeWithStep(1, 10, 2)) // [1, 3, 5, 7, 9]
console.log(rangeWithStep(11, 23, 3)) // [11, 14, 17, 20, 23]
console.log(rangeWithStep(5, 2, 1)) // [5, 4, 3, 2]
console.log(rangeWithStep(29, 2, 4)) // [29, 25, 21, 17, 13, 9, 5] 



//jawaban soal 3

var input = [
    ["0001", "Roman Alamsyah", "Bandar Lampung", "21/05/1989", "Membaca"],
    ["0002", "Dika Sembiring", "Medan", "10/10/1992", "Bermain Gitar"],
    ["0003", "Winona", "Ambon", "25/12/1965", "Memasak"],
    ["0004", "Bintang Senjaya", "Martapura", "6/4/1970", "Berkebun"]
] ;

for(var masuk of input){
    console.log(`nomer id : ${masuk[0]}  `);
    console.log(`Nama Lengkap : ${masuk[1]}`);
    console.log(`TTL : ${masuk[2]} ${masuk[3]}`);
    console.log(`Hobi : ${masuk[4]}`);
}

function balikKata(str) {
    var currentString = str;
    var newString = '';
   for (let i = str.length - 1; i >= 0; i--) {
     newString = newString + currentString[i];
    }
    
    return newString;
   }
   console.log(balikKata("Kasur Rusak")) // kasuR rusaK
   console.log(balikKata("SanberCode")) // edoCrebnaS
   console.log(balikKata("Haji Ijah")) // hajI ijaH
   console.log(balikKata("racecar")) // racecar
   console.log(balikKata("I am Sanbers")) // srebnaS ma I 
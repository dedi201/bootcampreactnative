import React from 'react'
import { StyleSheet, Text, View } from 'react-native'
import {createStackNavigator} from '@react-navigation/stack';
import AboutScreen from '../Tugas-13/AboutScreen';
import Login from '../Tugas-13/LoginScreen';
const Stack = createStackNavigator();

const Router = () => {
    return (
        <Stack.Navigator>
            <Stack.Screen component={AboutScreen} name="AboutScreen" options={{headerShown: false}}/>
            <Stack.Screen component={Login} name="Login" options={{headerShown: false}} />
        </Stack.Navigator>
    )
}  

export default Router;


import React from 'react';
import { Text, View, StyleSheet, Image } from 'react-native';

const AboutScreen = () => {
  return (
    <View>
      <View style={{ justifyContent: 'center', alignItems: 'center' }}>
        <Text
          style={{
            color: '#003366',
            fontSize: 36,
            // textAlign: 'center',
            fontWeight: 'bold',
            marginBottom: 12,
          }}>
          Tentang Saya
        </Text>
        <Image source={require('./assets/img/user.png')} style={styles.img} />
      </View>
      <View
        style={{
          justifyContent: 'center',
          alignItems: 'center',
          marginTop: 24,
        }}>
        <Text style={{ fontSize: 24, color: '#003366', fontWeight: 'bold' }}>
          Dedi Irawan
        </Text>
        <Text style={{ fontSize: 16, color: '#3EC6FF', fontWeight: 'bold' }}>
          React Native Developer
        </Text>
      </View>
      <View>
        <View style={{backgroundColor: '#EFEFEF',marginHorizontal: 8,
          paddingHorizontal: 8,
          paddingTop: 5,
          marginTop: 16,
          minHeight:140}}>
          <Text style={{ marginBottom: 8 }}>Portofolio</Text>
          <View
            style={{
              borderBottomColor: 'black',
              borderBottomWidth: 1,
              flexDirection: 'column',
            }}
          />
          <View style={styles.portofolio}>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Image
                source={require('./assets/img/gitlab.png')}
                style={{ width: 45, height: 45 }}
              />
              <Text style={{ textAlign: 'center', marginTop: 5 }}>
                @dedi201
              </Text>
            </View>
            <View style={{ justifyContent: 'center', alignItems: 'center' }}>
              <Image
                source={require('./assets/img/github.png')}
                style={{ width: 45, height: 45 }}
              />
              <Text style={{ textAlign: 'center', marginTop: 5 }}>
                @dediirawan201
              </Text>
            </View>
          </View>
        </View>
        <View style={{backgroundColor: '#EFEFEF',marginHorizontal: 8,
          paddingHorizontal: 8,
          paddingTop: 5,
          marginTop: 8,
          marginBottom: 9,}}>
          <Text style={{marginBottom:8}}>Hubungi Saya</Text>
          <View
            style={{
              borderBottomColor: 'black',
              borderBottomWidth: 1,
              flexDirection: 'column',
            }}
          />
          <View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginTop: 18,
              }}>
              <Image
                source={require('./assets/img/fb.png')}
                style={{ width: 39, height: 39, marginTop:16,marginBottom:26 }}
              />
              <Text style={{ marginLeft: 10 }}>@dediirawan201</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
                marginBottom:26
              }}>
              <Image
                source={require('./assets/img/logo-instagram.png')}
                style={{ width: 39, height: 39 }}
              />
              <Text style={{ marginLeft: 10 }}>@dediirawan201</Text>
            </View>
            <View
              style={{
                flexDirection: 'row',
                justifyContent: 'center',
                alignItems: 'center',
               marginBottom:26
              }}>
              <Image
                source={require('./assets/img/logo-twitter.png')}
                style={{ width: 39, height: 39 }}
              />
              <Text style={{ marginLeft: 10 }}>@dediirawan201</Text>
            </View>
          </View>
        </View>
      </View>
    </View>
  );
};

export default AboutScreen;

const styles = StyleSheet.create({
  portofolio: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginTop: 17,
  },
  img: { width: 200, height: 200 },
});

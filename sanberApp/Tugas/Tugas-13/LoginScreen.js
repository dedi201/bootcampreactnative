import React from 'react';
import { Text, View, StyleSheet,Image, TextInput,Button } from 'react-native';

const Login = () => {
  return (
    <View style={styles.container}>
    <Image  source={require('./assets/img/logo.png')} style={styles.logo}/>
    <View style={styles.inputContainer}>
    <Text style={{fontSize:24,marginBottom:40}}>Login</Text>
    <View style={styles.contentInput}>
    <Text style={styles.labelLogin}>UserName/Email</Text>
      <TextInput style={styles.input} placeholder="Masukan nama anda"/>
       <Text style={styles.labelPassword}>Password</Text>
      <TextInput style={styles.input} placeholder="Password"/>
      </View>
      <View style={styles.btn}>
      <Button title="Masuk" color="#3EC6FF"  />
      </View>
      <Text style={{marginVertical:16, fontSize:24, color:'#3EC6FF'}}>atau</Text>
      <View style={styles.btn}>
      <Button title="Daftar" color="#003366"  />
      </View>
    </View>
    </View>
  )
}

export default Login;

const styles = StyleSheet.create({
  container: {flex:1, },
  logo:{
    width:300,
    height:102,
    marginTop:60,
    marginLeft:20
  },
  inputContainer:{
    flex:1,
    justifyContent:'center',
    alignItems:'center',
  },
  input:{
    borderWidth:1,
    width:294,
    height:48,
    paddingLeft:10,
    
  },
  contentInput:{marginBottom:32},
  labelLogin:{
    marginBottom:4
  },
  labelPassword:{
    marginBottom:4,
    marginTop:16
  }, 
  btn:{width:140,
  overflow:'hidden',
  borderRadius:50,
  backgroundColor:'#003366'},
})
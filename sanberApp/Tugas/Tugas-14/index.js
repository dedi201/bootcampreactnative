import axios from 'axios'
import React, { useEffect, useState } from 'react'
import { StyleSheet, Text, View,TextInput, Button, Image, TouchableOpacity, } from 'react-native'


const Item = ({name,email,bidang}) => {
    return (
        <View style={styles.itemContainer}>
                <Image source={{uri:`https://i.pravatar.cc/150`}} style={styles.avatar}/>
                <View style={styles.desc}>
                    <Text style={styles.descName}>{name}</Text>
                    <Text style={styles.descEmail}>{email}</Text>
                    <Text style={styles.descBidang}>{bidang}</Text>
                </View>
                <Text style={styles.delete}>X</Text>
            </View>
    )
}
const localAPI = () => {
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [bidang, setBidang] = useState('');
    const [users, setUsers] = useState([]);
    const kuliah ="malam";

    useEffect(() => {
        getData();
    },[]);

    const submit = () => {
    const data ={
        name,
        email,
        bidang,
        kuliah
    }
    console.log('data before send', data);
    axios.post('https://achmadhilmy-sanbercode.my.id/api/v1/news',data)
    .then(res => {
        console.log('res',res);
        setName("");
        setEmail("");
        setBidang("");
        getData();
    })
}
	
	const getData = () => {
        axios.get('https://achmadhilmy-sanbercode.my.id/api/v1/news')
        .then(res => {
            console.log('res: ', res);
            setUsers(res.data);
        })
    }

    return (
        <View style={styles.container}>
            <Text style={styles.title}>Local API</Text>
            
            <TextInput placeholder="Nama lengkap" style={styles.input} value={name} onChangeText={(value) => setName(value)}/>
            <TextInput placeholder="Email" style={styles.input} value={email} onChangeText={(value) => setEmail(value)}/>
            <TextInput placeholder="Bidang" style={styles.input} value={bidang} onChangeText={(value) => setBidang(value)}/>
            <Button title="simpan"  onPress={submit}/>
            <View style={styles.line} />
            {users.map(user => {
                return <Item key={user.id} name={user.name}  />
            })}
            <Item />
            <Item />
            <Item />
        </View>
    )
}


export default localAPI;

const styles = StyleSheet.create({
    container:{padding:10},
    title:{textAlign:'center',marginBottom: 20},
    line:{height:2,backgroundColor:'black',marginVertical:3},
    input:{borderWidth:1, marginBottom:12,borderRadius:25,paddingLeft:10},
    avatar:{width:100, height:100, borderRadius:100},
    itemContainer:{flexDirection:'row', marginBottom:10},
    desc:{marginLeft:20,flex:1},
    descName:{fontSize:20,fontWeight:'bold'},
    descEmail:{fontSize:16},
    descBidang:{fontSize:12,marginTop:8},
    delete:{fontSize:20, fontWeight:'bold',color:'red'}
})

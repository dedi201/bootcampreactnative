// var namaMhs = 'Dedi Irawan';
// umur = 25;
// var lulus = true;
// var semesterIPK = [2.90, 3.04, 3.33, 3.50];

// function IPKomulatif(semesterIPK) {
//     var total =0;
//     for(var i = 0; i < semesterIPK.length; i++){
//         total += semesterIPK[i];
//     }
//     return total/semesterIPK.length;
// }

// console.log(IPKomulatif())
var arr = ["budi",1,true];
const dataMhs = {
    nama: "dedi",
    semester: 2,
    lulus: true,
    "kelas dari":"teknik komputer",
    organisasi:{
        beladiri:'karate',
        himpunan:'HMJTK',
        keagamaan:"badaris"
    },
    kota: function(kotaku){
        return "halo nama saya " + this.nama + " dari " + kotaku
    }
}

//object didalam array
var arryObj = [
    {nama:"dedi",status:"sudah menikah", lulus:true,"tempat tanggal lahir":"BEkasi, 20 januari 1994"},
    {nama:"budi",status:"belum menikah", lulus:false,"tempat tanggal lahir":"BEkasi, 20 januari 2001"}
]

console.log(dataMhs.nama,dataMhs.lulus)
console.log(dataMhs.kota('BEkasi'))
console.log(dataMhs.organisasi.keagamaan)
console.log(dataMhs["kelas dari"])
//pemanggilan array
console.log(typeof arr);
console.log(arr[1])
console.log(arryObj[0])
//foreach
arryObj.forEach(function(item){
    console.log(item.nama);
});
//map(membuar array baru)
var status =arryObj.map(function(item){
    return item.lulus
})

console.log(status)

//filter menyaring data yang akan ditampilkan
var filterObj = arryObj.filter(function(item){
    return item.nama != "budi";
})

console.log(filterObj);